class Solution
{
    //Function to merge the arrays.
    public static void merge(long arr1[], long arr2[], int n, int m) 
    {
        
        int i = 1;
        
        for(i = 1 ; i <= n && i <= m ; i++)
        {
            if(arr1[n-i] > arr2[i-1])
            {
                long temp = arr1[n-i];
                arr1[n-i] = arr2[i-1];
                arr2[i-1] = temp; 
            }
            
        }
        
        
        Arrays.sort(arr1);
        Arrays.sort(arr2);
            
    }
}
