class Solution {
    int MissingNumber(int array[], int n) {
        
        boolean[] marking = new boolean[n];
        
        Arrays.fill(marking, Boolean.FALSE);
        
        for(int i : array)
        {
            marking[i-1] = true;
        }
        
        for(int flag = 0 ; flag < n ; flag++)
        {
            if(marking[flag] == false )
            {
                // System.out.println(flag);
                return flag + 1 ;
            }
        }
        return 0;
    }
}
