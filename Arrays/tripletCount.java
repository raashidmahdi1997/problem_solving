
class Solution {
    int countTriplet(int arr[], int n) {
       
       
       
       int tripleCount = 0;
       
       HashMap<Integer, Integer> indexMapping = new HashMap<Integer, Integer>();
       
       for(int i = 0 ; i < arr.length ; i++)
       {
           indexMapping.put(arr[i] , i);
       }
       
    //   Arrays.sort(arr);
       
       for(int i = 0 ; i < arr.length - 1 ; i++)
       {
           for(int j = i+1 ; j < arr.length ; j++)
           {
                int sum = arr[i] + arr[j];
                // System.out.println(indexMapping.get(sum));
                if(indexMapping.get(sum) != null)
                {
                    tripleCount++;
                }
           }
           
       }
       
       return tripleCount;
    }
}
