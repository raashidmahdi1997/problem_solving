
class Solution{

    // arr: input array
    // n: size of array
    //Function to find the sum of contiguous subarray with maximum sum.
    long maxSubarraySum(int arr[], int n){
        
       long maxSum = Integer.MIN_VALUE;
        
        int sum = 0;
        int startIndex = 0;
        int endIndex = 0;
        
        for(int i = 0 ; i < arr.length ; i++)
        {
            sum = sum + arr[i];
            
            if(maxSum < sum)
            {
                maxSum = sum;
            }
            if(sum < 0)
            {
                sum = 0;
            }
        }
        
        return maxSum;
        
    }
    
}

