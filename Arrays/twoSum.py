
class Solution:
    def twoSum(self, nums: List[int], target: int) -> List[int]:
        
        solutions = []
    
        data = []

        obj = {
            "v" : 0,
            "p" : 0
        }

        for i in range(0 , len(nums)):

            obj["v"] = nums[i]
            obj["p"] = i
            data.append(obj.copy())


        data.sort(key=lambda x: x["v"], reverse= False)

        del(nums)

        left = 0
        right = len(data) - 1

        # print(data)

        while(left < right):

            # print(data[left] , data[right])

            if data[left]["v"] + data[right]["v"] > target:

                right -= 1

            elif data[left]["v"] + data[right]["v"] == target:

                solutions.append(data[left]["p"])
                solutions.append(data[right]["p"])

                left += 1
                right -= 1

            else:
                left += 1

        return solutions


