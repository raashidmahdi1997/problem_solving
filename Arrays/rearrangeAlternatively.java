
class Solution{
    
    // temp: input array
    // n: size of array
    //Function to rearrange  the array elements alternately.
    public static void rearrange(long arr[], int n){
        
        // Your code here
        
        long[] temp = new long[n];
        

        for(int i = 0 ; i < n ; i++)
        {
            temp[i] = arr[i];
        }
        
        for(int i = 0 ; i < n/2 ; i++)
        {
            arr[(i)*2 + 1] = temp[i];
        }
        
        for(int i = n ; i > n/2 ; i--)
        {
            arr[(n-i)*2] = temp[i-1];
        }
        
        
    }
    
}


